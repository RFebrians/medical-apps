import { createContext } from "react";

export const AppContext = createContext({
    defaultDoctor: {
        name: "John Test",
        id: "40NlIuRASffNoTUU9qdm"
    }
});