import { initializeApp } from "firebase/app";

const firebaseConfig = {
    apiKey: "AIzaSyBkqOejg3Je6m75n78ho4S-gdLX3E7VOzw",
  authDomain: "forwardproject-2bedf.firebaseapp.com",
  databaseURL: "https://forwardproject-2bedf-default-rtdb.firebaseio.com",
  projectId: "forwardproject-2bedf",
  storageBucket: "forwardproject-2bedf.appspot.com",
  messagingSenderId: "551104926569",
  appId: "1:551104926569:web:ff8ff7e86638c6207223f8",
  measurementId: "G-WML5K0SE0C"
};

const app = initializeApp(firebaseConfig);

export default app;