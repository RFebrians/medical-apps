import app from "../../services/firebase/firebase";
import { iDoctors } from "./DoctorSchema";
import { useCollection, useDocument } from 'react-firebase-hooks/firestore'
import {
    getFirestore,
    collection,
    query,
    DocumentData,
    doc,
    deleteDoc,
    addDoc,
    updateDoc,
    where
} from "firebase/firestore";


const Doctors = {

    db: getFirestore(app),

    findAll() {
        return useCollection<DocumentData | iDoctors>(
            query(collection(this.db,'doctors'))
        );
    },

    findById(apptId: string) {
        return useDocument<DocumentData | iDoctors>(
            doc(this.db, 'doctors', apptId)
        );
    },

    findByPatientId(patientId: string) {
        return useCollection<DocumentData | iDoctors>(
            query(collection(this.db, "doctors"), where("patient_id", "==", patientId))
        );
    },

    async deleteById(apptId: string) {
        await deleteDoc(doc(this.db, 'doctors', apptId));
    },

    async updateById(apptId: string, data: Object | iDoctors) {
        await updateDoc(doc(this.db, 'doctors', apptId), data);
    },

    async create(data: Object | iDoctors) {

        function instanceOfPatient(object: any): object is iDoctors {
            return (
                'patient_id' in object &&
                'doctor_id' in object &&
                'date' in object &&
                'treatment' in object &&
                'time' in object
            );
        }

        if (!instanceOfPatient(data)) {
            alert("Cannot leave empty fields");
            return;
        }

        await addDoc(collection(this.db, 'doctors'), data);
    }
}

export default Doctors;