import { Timestamp } from 'firebase/firestore'

export interface iDoctors { 
    patient_id: number
    doctor_id: number
    date: Timestamp
    time: string
    treatment: string
}