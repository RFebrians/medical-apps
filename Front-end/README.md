# Medical App Front-end
Alpha Development

### Login Credentials

##### GMAIL
```
tests@gmail.com
```
##### PASSWORD
```
123456
```

### Overview

This pack includes the following modules:
  - Manage appointments.
  - Manage patients personal information and status of application.
  - Automatic database updates.
  - Error handling.
  - Usage effiency.

### Features

- Login view, including "hide password" option
- Appointments filtering by date
- Add/Update new patients
- Add new appointments
- Search bar for each table
- Update patient information
- Logout option

### Development

- React (with Typescript)
- Firebase <b> 9.6 </b>
- Material UI kit 
- Create-React-App

> Mock DB in persistence folder
