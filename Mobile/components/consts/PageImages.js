const pageImages = [
  {
    id: "1",
    name: "Appointment",
    image: require("../assets/Appointment.png"),
  },
  {
    id: "2",
    name: "Map",
    image: require("../assets/Maps.png"),
  },
];

export default pageImages;
