import React from "react";
import { StyleSheet, View } from "react-native";
import SearchMap from "../subcomponents/SearchMap";

function MapScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <SearchMap />
    </View>
  );
}

export default MapScreen;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  header: {
    marginTop: 35,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 20,
  },
  textHeader: {
    fontSize: 24,
    fontWeight: "bold",
  },
});
