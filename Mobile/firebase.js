// Import the functions you need from the SDKs you need
import firebase from "firebase";
import "firebase/database";
//import { getFirestore } from 'firebase/firestore';

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBkqOejg3Je6m75n78ho4S-gdLX3E7VOzw",
  authDomain: "forwardproject-2bedf.firebaseapp.com",
  databaseURL: "https://forwardproject-2bedf-default-rtdb.firebaseio.com",
  projectId: "forwardproject-2bedf",
  storageBucket: "forwardproject-2bedf.appspot.com",
  messagingSenderId: "551104926569",
  appId: "1:551104926569:web:52f568ed99c121457223f8",
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);

// Initialize Firebase Authentication and get a reference to the service
const auth = firebase.auth();

// Initialize Firebase Realtime Database / User 
const db = firebase.database();

// Init Firestore for Doctor List
//const firestore = getFirestore();

export { auth, db };
